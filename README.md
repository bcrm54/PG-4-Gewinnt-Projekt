Vier-Gewinnt

Wie der Name beschreibt handelt es sich hierbei um eine Simulation des klassischen 4-Gewinnt.
Das Spielfeld ist sechs Felder hoch und sieben Felder breit. 
Beide Spieler (Rot und Gelb) setzen abwechselnd ihre roten bzw. gelben Spielsteine in dieses Feld.
Die Spielsteine werden gesetzt, indem das oberste Feld einer Spalte angeklickt wird. Der Spielstein "fällt" daraufhin in die unterste Zeile der Spalte,
oder auf den obersten Spielstein dieser Spalte, falls dort schon ein Spielstein liegt.
Gewonnen hat der Spieler, der es zuerst schafft ununterbrochen vom Gegenspieler vier Spielsteine waagerecht, senkrecht oder diagonal anzuordnen.

In unserer Version von Vier-Gewinnt werden die Spielsteine, die den Sieg eines Spielers herbeiführen, andersfarbig aufblinken, damit ohne großes Suchen
festgestellt werden kann, wer nun gewonnen hat.

Die größte Schwierigkeit beim Programmieren wird die Überprüfung, ob nun vier Spielsteine eines Spielers in einer waagerechten, 
senkrechten oder diagonalen Reihe liegen, die zum Sieg eines Spieler führen.